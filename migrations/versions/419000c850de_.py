"""empty message

Revision ID: 419000c850de
Revises: 
Create Date: 2019-01-19 01:52:30.519324

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '419000c850de'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('people', sa.Column('img_src', sa.String(length=64), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('people', 'img_src')
    # ### end Alembic commands ###
