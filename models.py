from app import db, ma

class People(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(64))
  img_src = db.Column(db.String(64))

  def __init__(self, *args, **kwargs):
    super(People, self).__init__(*args, **kwargs)

  def __repr__(self):
    return '<People id: {}, name: {}'.format(self.id, self.name )

class PeopleSchema(ma.Schema):
  class Meta:
    fields = ('id', 'name', 'img_src')

people_schema = PeopleSchema(strict=True)
peoples_schema = PeopleSchema(many=True, strict=True)


class Kinder(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  img_src = db.Column(db.String(64))

  def __init__(self, *args, **kwargs):
    super(Kinder, self).__init__(*args, **kwargs)

  def __repr__(self):
    return '<Kinder id: {}, img_src: {}'.format(self.id, self.img_src )

class KinderSchema(ma.Schema):
  class Meta:
    fields = ('id', 'img_src')
    
kinder_schema = KinderSchema(strict=True)
kinders_schema = KinderSchema(many=True, strict=True)


class KinderInPeople(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  kinder_id = db.Column(db.Integer, db.ForeignKey('kinder.id'), nullable=False)
  people_id = db.Column(db.Integer, db.ForeignKey('people.id'), nullable=False)
  amount = db.Column(db.Integer)

  def __init__(self, *args, **kwargs):
    super(KinderInPeople, self).__init__(*args, **kwargs)

  def __repr__(self):
    return '<KinderInPeople id: {}, people_id: {}, kinder_id: {}, amount: {}'.format(
      self.id, self.people_id, self.kinder_id,self.amount)

class KinderInPeopleSchema(ma.Schema):
  class Meta:
    fields = ('id', 'kinder_id', 'people_id', 'amount')
    
kinder_in_people_schema = KinderInPeopleSchema(strict=True)
kinders_in_peoples_schema = KinderInPeopleSchema(many=True, strict=True)


