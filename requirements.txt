Click==7.0
Flask==1.0.2
Flask-Cors==3.0.7
flask-marshmallow==0.9.0
Flask-SQLAlchemy==2.3.2
itsdangerous==1.1.0
Jinja2==2.10
jsonpickle==1.0
MarkupSafe==1.1.0
marshmallow==2.17.0
marshmallow-sqlalchemy==0.15.0
mysql-connector==2.1.6
six==1.12.0
SQLAlchemy==1.2.16
Werkzeug==0.14.1
