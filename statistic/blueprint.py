from flask import Blueprint, jsonify, json, request
import pprint

from models import *

statistic = Blueprint('statistic', __name__, )

@statistic.route('/get_all', methods=['GET'])
def get_all():
  # Get/serialize peoples
  peoples = People.query.all()
  peoples_res = peoples_schema.dump(peoples)
  
  # Get/serialize kinders
  kinders = Kinder.query.all()
  kinders_res = kinders_schema.dump(kinders)

   # Get/serialize kinders_in_peoples
  kinders_in_peoples = KinderInPeople.query.all()
  kinders_in_peoples_res = kinders_in_peoples_schema.dump(kinders_in_peoples)

  # Jsonify data
  res = {}
  res['peoples'] = peoples_res.data
  res['kinders'] = kinders_res.data
  res['kinders_in_peoples'] = kinders_in_peoples_res.data
  json_res = jsonify(res)
  return json_res
  

@statistic.route('/get_peoples', methods=['GET'])
def get_peoples():
  peoples = People.query.all()
  result = peoples_schema.dump(peoples)
  return jsonify(result.data)


@statistic.route('/get_kinders', methods=['GET'])
def get_kinders():
  kinders = Kinder.query.all()
  result = kinders_schema.dump(kinders)
  return jsonify(result.data)


@statistic.route('/get_kinders_in_peoples', methods=['GET'])
def get_kinders_in_peoples():
  kinders_in_peoples = KinderInPeople.query.all()
  result = kinders_in_peoples_schema.dump(kinders_in_peoples)
  return jsonify(result.data)


@statistic.route('/update_kinder_in_people/<id>', methods=['PUT'])
def update_kinder_in_people(id):
  cur_kinder = KinderInPeople.query.get(id)
  amount = request.json['amount']
  cur_kinder.amount = amount
  db.session.commit()
  return kinder_in_people_schema.jsonify(cur_kinder)

@statistic.route('/update_name/<id>', methods=['PUT'])
def update_name(id):
  cur_people = People.query.get(id)
  name = request.json['name']
  cur_people.name = name
  db.session.commit()
  return people_schema.jsonify(cur_people)

@statistic.route('/update_img/<id>', methods=['PUT'])
def update_img(id):
  cur_people = People.query.get(id)
  img = request.json['img']
  cur_people.img_src = img
  db.session.commit()
  return people_schema.jsonify(cur_people)
