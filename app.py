from flask import Flask
from config import Configuration 
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS

import os

app = Flask(__name__)
app.config.from_object(Configuration)
CORS(app)

db = SQLAlchemy(app)
ma = Marshmallow(app)