from app import app 
from app import db

from statistic.blueprint import statistic

import view

app.register_blueprint(statistic, url_prefix='/')

if __name__ == '__main__':
  app.run(debug=True)